﻿/*
  DatabaseActivator.cs
  Mission: Invasion
  Created by Rohun Banerji on Dec 7/2014
  Copyright (c) 2014 Rohun Banerji. All rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DatabaseActivator : MonoBehaviour 
{
    [SerializeField]
    private HullTable hullTable;
    [SerializeField]
    private ComponentTable compTable;
    [SerializeField]
    private GlobalVars globalVars;
    [SerializeField]
    private SaveFilesConfig saveFilesConfig;
    [SerializeField]
    private TagsAndLayers tagsAndLayers;
    [SerializeField]
    private GameConfig gameConfig;
    [SerializeField]
    private GalaxyConfig galaxyConfig;
    [SerializeField]
    private BlueprintTemplates bpTemplates;
    [SerializeField]
    private ResourceManager resourceManager;
    [SerializeField]
    private PlayerShipConfig playerShipConfig;

	
}
