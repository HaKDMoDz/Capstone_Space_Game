﻿using System.Xml;
using System.Xml.Serialization;

public class FogSquare
{
    [XmlAttribute("x")]
    public string x;

    [XmlAttribute("y")]
    public string y;

}