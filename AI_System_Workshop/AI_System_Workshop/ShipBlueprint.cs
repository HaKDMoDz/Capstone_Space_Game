﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_System_Workshop
{
    /// <summary>
    /// This is a stub to connect to the ShipBlueprint in Unity
    /// </summary>
    class ShipBlueprint
    {
        public ShipBlueprint()
        {
            Console.WriteLine("ShipBlueprint Created");
            Console.WriteLine("This should hook into Unity's ShipBlueprint class");
        }
    }
}
