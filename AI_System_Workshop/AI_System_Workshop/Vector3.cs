﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_System_Workshop
{
    /// <summary>
    /// This is to stub Unity's Vector3
    /// </summary>
    class Vector3
    {
        public Vector3()
        {
            Console.WriteLine("Vector3 Created");
            Console.WriteLine("This should hook into Unity's Vector3 class");
        }
    }
}
