﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_System_Workshop
{
    /// <summary>
    /// This is a stub to connect to the Unit in Unity
    /// </summary>
    class Unit
    {
        public Unit()
        {
            Console.WriteLine("Unit Created");
            Console.WriteLine("This should hook into Unity's Unit class");
        }
    }
}
